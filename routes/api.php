<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/stakeholders', 'StakeholdersController@store')->name('api.stakeholders')->middleware('client');
Route::get('/vipslist', 'StakeholdersController@vipslist')->name('api.vipslist')->middleware('client');
Route::get('/stakeholderslist', 'StakeholdersController@stakeholderslist')->name('api.stakeholderslist')->middleware('client');

Route::get('/stakeholders/{stakeholder}', 'StakeholdersController@show')->name('api.show')->middleware('client');
Route::get('/stakeholders/{stakeholder}/edit', 'StakeholdersController@edit')->name('api.edit')->middleware('client');

Route::get('/specializations', 'SpecializationController@index')->middleware('client');

Route::get('/academictitles', 'AcademictitleController@index')->middleware('client');

Route::get('/sections', 'SectionsController@index')->middleware('client');

Route::get('/departments', 'DepartmentsController@index')->middleware('client');

Route::get('/courses', 'CoursesController@index')->middleware('client');

Route::put('stakeholders/{id}/cancel', [
    'uses' => 'StakeholdersController@cancel'
])->middleware('client');

Route::put('stakeholders/{id}', [
    'uses' => 'StakeholdersController@update'
])->middleware('client');