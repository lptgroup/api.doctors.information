<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/create-gynecology', 'GynecologyController@create')->name('create-gynecology');
Route::post('/post-gynecology', 'GynecologyController@store')->name('post-gynecology');
Route::get('/edit-gynecology/{gynecology}', 'GynecologyController@edit')->name('edit-gynecology');
Route::post('/update-gynecology/{gynecology}', 'GynecologyController@update')->name('update-gynecology');
Route::get('/gynecology-list', 'GynecologyController@index')->name('gynecology-list');
Route::delete('/delete-gynecology/{gynecology}', 'GynecologyController@delete')->name('delete-gynecology');

Route::get('/create-sections-gynecology', 'GynecologySectionController@create')->name('create-sections-gynecology');
Route::post('/post-sections-gynecology', 'GynecologySectionController@store')->name('post-sections-gynecology');
Route::get('/gynecology-sections-list', 'GynecologySectionController@index')->name('gynecology-sections-list');
Route::get('/edit-sections-gynecology/{gynecologysection}', 'GynecologySectionController@edit')->name('edit-sections-gynecology');
Route::post('/update-sections-gynecology/{gynecologysection}', 'GynecologySectionController@update')->name('update-sections-gynecology');
Route::delete('/delete-sections-gynecology/{gynecologysection}', 'GynecologySectionController@delete')->name('delete-sections-gynecology');

Route::get('/create-topics-gynecology', 'GynecologyTopicController@create')->name('create-topics-gynecology');
Route::post('/post-topics-gynecology', 'GynecologyTopicController@store')->name('post-topics-gynecology');
Route::get('/gynecology-topics-list', 'GynecologyTopicController@index')->name('gynecology-topics-list');
Route::get('/edit-topics-gynecology/{gynecologytopic}', 'GynecologyTopicController@edit')->name('edit-topics-gynecology');
Route::post('/update-topics-gynecology/{gynecologytopic}', 'GynecologyTopicController@update')->name('update-topics-gynecology');
Route::delete('/delete-topics-gynecology/{gynecologytopic}', 'GynecologyTopicController@delete')->name('delete-topics-gynecology');
