@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h2>Dodaj dane firmy ginekologicznej</h2><br/>
        <div class="container">
            <div class="row">
                @if(Session::has('createGynecology'))
                    <div class="alert alert-success text-dark {{ Session::get('createGynecology') }}">{{ Session::get('createGynecology') }}</div>
                @endif
            </div>
        </div>
        
        <form method="post" action="{{ route('post-gynecology') }}">
        {{ csrf_field() }}
            <div class="container">
                <div class="form-group row">
                    <label for="company" class="col-sm-2 col-form-label text-left pt-3">Nazwa firmy:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="company" name="company" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="rx" class="col-sm-2 col-form-label text-left pt-3">RX:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="rx" name="rx" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="otc" class="col-sm-2 col-form-label text-left pt-3">OTC:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="otc" name="otc" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-sm-2 col-form-label text-left pt-3">Ulica:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="address" name="address" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="post_code" class="col-sm-2 col-form-label text-left pt-3">Kod pocztowy:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="post_code" name="post_code" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="city" class="col-sm-2 col-form-label text-left pt-3">Miasto:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="city" name="city" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone1" class="col-sm-2 col-form-label text-left pt-3">Tel. stacjonarny:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="phone1" name="phone1" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone2" class="col-sm-2 col-form-label text-left pt-3">Tel. komórkowy:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="phone2" name="phone2" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="website" class="col-sm-2 col-form-label text-left pt-3">WWW:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="website" name="website" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label text-left pt-3">Email:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="email" name="email" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label text-left pt-3">Uwagi:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="description" name="description" value="">
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block">Dodaj</button>
                </div> 
            </div>
        </form>
    </div>
</div>
@endsection