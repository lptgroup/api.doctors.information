@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h2>Dodaj dane firmy ginekologicznej</h2><br/>
        <div class="container">
            <div class="row">
                @if(Session::has('createGynecology'))
                    <div class="alert alert-success text-dark {{ Session::get('createGynecology') }}">{{ Session::get('createGynecology') }}</div>
                @endif
            </div>
        </div>
        
        <form method="post" action="{{ route('post-topics-gynecology') }}">
        {{ csrf_field() }}
            <div class="container">
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label text-left pt-3">Nazwa sekcji:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="name" name="name" value="">
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block">Dodaj</button>
                </div> 
            </div>
        </form>
    </div>
</div>
@endsection