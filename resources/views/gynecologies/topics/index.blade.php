@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row mx-5">
        <div class="col-sm-12">
            <h2>Tematy ginekologiczne</h2>
        </div>
        <div class="col-sm-12">
            @if(Session::has('destroyGynecology'))
                <div class="alert alert-danger text-dark {{ Session::get('destroyGynecology') }}">{{ Session::get('destroyGynecology') }}</div>
            @endif
        </div>
        <div class="col-sm-12">
            <a href="{{ action('GynecologyTopicController@create') }}" class="btn btn-success btn-block border border-success">Dodaj</a>  
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <td scope="col"><b>Nazwa tematu</b></td>
                    <td scope="col"><b>Edycja</b></td>
                    <td scope="col"><b>Usuń</b></td>
                </tr>
            </thead>
            <tbody>
                @foreach($topics as $section)
                <tr>
                    <td>{{ $section->name }}</td>
                    <td>
                        <a href="{{ action('GynecologyTopicController@edit', $section->id) }}" class="btn btn-warning border border-warning">Edytuj</a>  
                    </td>
                    <td>
                        <form method="post" action="{{ route('delete-topics-gynecology', $section->id) }}">
                            {{ method_field('DELETE') }}
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-danger border border-warning">Usuń</button>  
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection