@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row mx-5">
        <div class="col-sm-12">
            <h2>Firm ginekologiczne</h2>
        </div>
        <div class="col-sm-12">
            @if(Session::has('destroyGynecology'))
                <div class="alert alert-danger text-dark {{ Session::get('destroyGynecology') }}">{{ Session::get('destroyGynecology') }}</div>
            @endif
        </div>
        <div class="col-sm-12">
            <a href="{{ action('GynecologyController@create') }}" class="btn btn-success btn-block border border-success">Dodaj</a>  
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <td scope="col"><b>Nazwa firmy</b></td>
                    <td scope="col"><b>RX</b></td>
                    <td scope="col"><b>OTC</b></td>
                    <td scope="col"><b>Ulica</b></td>
                    <td scope="col"><b>Kod pocztowy</b></td>
                    <td scope="col"><b>Miasto</b></td>
                    <td scope="col"><b>Tel. stacjonarny</b></td>
                    <td scope="col"><b>Tel. komórkowy</b></td>
                    <td scope="col"><b>WWW</b></td>
                    <td scope="col"><b>Email</b></td>
                    <td scope="col"><b>Uwagi</b></td>
                    <td scope="col"><b>Edycja</b></td>
                    <td scope="col"><b>Usuń</b></td>
                </tr>
            </thead>
            <tbody>
                @foreach($gynecologies as $gynecology)
                <tr>
                    <td>{{ $gynecology->company }}</td>
                    <td>{{ $gynecology->rx }}</td>
                    <td>{{ $gynecology->otc }}</td>
                    <td>{{ $gynecology->address }}</td>
                    <td>{{ $gynecology->post_code }}</td>
                    <td>{{ $gynecology->city }}</td>
                    <td>{{ $gynecology->phone1 }}</td>
                    <td>{{ $gynecology->phone2 }}</td>
                    <td>{{ $gynecology->website }}</td>
                    <td>{{ $gynecology->email }}</td>
                    <td>{{ $gynecology->description }}</td>
                    <td>
                        <a href="{{ action('GynecologyController@edit', $gynecology->id) }}" class="btn btn-warning border border-warning">Edytuj</a>  
                    </td>
                    <td>
                        <form method="post" action="{{ route('delete-gynecology', $gynecology->id) }}">
                            {{ method_field('DELETE') }}
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-danger border border-warning">Usuń</button>  
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection