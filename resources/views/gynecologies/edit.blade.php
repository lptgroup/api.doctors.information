@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h2>Edycja danych firmy {{ $gynecology->company }}</h2><br/>
        <div class="container">
            <div class="row">
                @if(Session::has('editGynecology'))
                    <div class="alert alert-success text-dark {{ Session::get('editGynecology') }}">{{ Session::get('editGynecology') }}</div>
                @endif
            </div>
        </div>
        
        <form method="post" action="{{ route('update-gynecology', $gynecology->id) }}">
        {{ csrf_field() }}
            <div class="container">
                <div class="form-group row">
                    <label for="company" class="col-sm-2 col-form-label text-left pt-3">Nazwa firmy:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="company" name="company" value="{{ $gynecology->company }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="rx" class="col-sm-2 col-form-label text-left pt-3">RX:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="rx" name="rx" value="{{ $gynecology->rx }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="otc" class="col-sm-2 col-form-label text-left pt-3">OTC:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="otc" name="otc" value="{{ $gynecology->otc }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-sm-2 col-form-label text-left pt-3">Ulica:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="address" name="address" value="{{ $gynecology->address }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="post_code" class="col-sm-2 col-form-label text-left pt-3">Kod pocztowy:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="post_code" name="post_code" value="{{ $gynecology->post_code }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="city" class="col-sm-2 col-form-label text-left pt-3">Miasto:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="city" name="city" value="{{ $gynecology->city }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone1" class="col-sm-2 col-form-label text-left pt-3">Tel. stacjonarny:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="phone1" name="phone1" value="{{ $gynecology->phone1 }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone2" class="col-sm-2 col-form-label text-left pt-3">Tel. komórkowy:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="phone2" name="phone2" value="{{ $gynecology->phone2 }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="website" class="col-sm-2 col-form-label text-left pt-3">WWW:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="website" name="website" value="{{ $gynecology->website }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label text-left pt-3">Email:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="email" name="email" value="{{ $gynecology->email }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label text-left pt-3">Uwagi:</label>
                    <div class="col-sm-10 pt-3">
                        <input type="text" class="form-control" id="description" name="description" value="{{ $gynecology->description }}">
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block">Aktualizuj</button>
                </div> 
            </div>
        </form>
    </div>
</div>
@endsection