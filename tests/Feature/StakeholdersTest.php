<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Models\Section;
use App\Models\Specialization;
use App\Models\Course;

class StakeholdersTest extends TestCase
{
    use RefreshDatabase;
    use WithoutMiddleware;

    /** @test */
    public function admin_can_add_vip()
    {
        $section1 = new Section();
        $section1->section = 'Sekcja Jaskry';
        $section1->save();

        $section2 = new Section();
        $section2->section = 'Sekcja Chirurgii Zacmy i Chirurgii Refrakcyjnej';
        $section2->save();

        $specialization1 = new Specialization();
        $specialization1->specialization = 'Anestezjologia i intensywna terapia';
        $specialization1->save();

        $specialization2 = new Specialization();
        $specialization2->specialization = 'Balneologia i medycyna fizykalna';
        $specialization2->save();

        $course1 = new Course();
        $course1->name = 'Anestezjologia i intensywna terapia';
        $course1->price = 145.50;
        $course1->save();

        $course2 = new Course();
        $course2->name = 'Balneologia i medycyna fizykalna';
        $course1->price = 145.50;
        $course2->save();

        $payload = [
            'stakeholder' => [
                'first_name' => 'Adam',
                'last_name' => 'adamski',
                'email' => 'sally@example.com',
                'pesel' => '90879810252',
                'pwz' => '4789321',
                'phone' => '502111777',
                'sex' => 'M',
                'city' => 'Łódź',
                'post_code' => '90-666',
                'address' => 'ul. Kwitkowa 99/202',
                'academic_title_id' => 1,
                'department_id' => 7
            ],

            'selected_courses' => [
                $course1->id, 
                $course2->id
            ],

            'congress' => [
                'stakeholder_id' => 1,
                'congress_name' => 'Zjazd okulistów Polskich Katowice 2018',
                'status' => 'vip',
                'payment' => 850,
                'payment_date' => '2018-02-02',
                'payer_type_id' => 1,
                'course_id' => 1
            ],

            'selected_sections' => [
                $section1->id, 
                $section2->id 
            ],

            'confirmation' => [
                'stakeholder_id' => 1,
                'description' => 'Zgoda na przetwarzanie danych'
            ],

            'membership' => [
                'stakeholder_id' => 1,
                'is_member' => 1,
                'date_from' => '2018-01-02'
            ],

            'affiliation' => [
                'stakeholder_id' => 1,
                'affiliation_name' => 'Afiliacja 1',
                'affiliation_city' => 'Katowice',
                'affiliation_post_code' => '90-200',
                'affiliation_address' => 'Gdańska 126/128'
            ],

            'payer' => [
                'congress_id' => 1,
                'type' => 'firma',
                'name' => 'Novartis'
            ],

            'selected_specializations' => [
                $specialization1->id,
                $specialization2->id 
            ],
        ];

        $response = $this->post(\URL::Route('api.stakeholders', $payload));

        $this->assertDatabaseHas('stakeholders', $payload['stakeholder']);

        $this->assertDatabaseHas('courses', $payload['course']);

        $this->assertDatabaseHas('congresses', $payload['congress']);

        $this->assertDatabaseHas('stakeholder_sections', ['stakeholder_id' => 1, 'section_id' => $section1->id]);

        $this->assertDatabaseHas('stakeholder_specializations', ['stakeholder_id' => 1, 'specialization_id' => $specialization1->id]);

        $this->assertDatabaseHas('confirmations', $payload['confirmation']);

        $this->assertDatabaseHas('memberships', $payload['membership']);

        $this->assertDatabaseHas('affiliations', $payload['affiliation']);

        $this->assertDatabaseHas('payers', $payload['payer']);
    }
}
