<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            SpecializationTableSeeder::class,
            AcademicTitleTableSeeder::class,
            SectionsTableSeeder::class,
            DepartmentsTableSeeder::class,
            CoursesTableSeeder::class
        ]);
    }
}
