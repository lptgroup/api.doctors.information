<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([
            [
                'name' => 'Podstawowe techniki w okuloplastyce. Anna Maria Bilkiewicz-Pawelec, Paweł Jędrzejewski Okulistyka Anna Maria Bilkiewicz-Pawelec',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Zabiegi laserem Nd:YAG - technika kapsulotomii, irydotomii i witreolizy. Andrzej Dmitriew, Marta Pietruszyńska, Agata Brązert',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Retinopatia wcześniaków- patogeneza, diagnostyka i współczesne metody leczenia. Bogumiła Wójcik- Niklewska, Erita Filipek',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Jednoczesny zabieg fotokeratektomii refrakcyjnej w oparciu o topografię rogówki i analizę czoła fali oraz sieciowania włókien kolagenowych w leczeniu stożka rogówki. Beata Bubała-Stachowicz Kierownik: Prof. Ewa Mrukwa - Kominek',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Nowe postacie leku w okulistyce. Marek Prost, Renata Jachowicz, Jaromir Wasyluk',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Chirurgiczne leczenie schorzeń rogówki. Justyna Izdebska, Anna Kurowska',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'IStent - nowa terapia w jaskrze. Iwona Filipecka Okulus Plus Centrum Okulistyki i Optometrii Bielsko-Biała',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Chirurgia plastyczna wrodzonych i pourazowych ubytków tęczówki. Adam Cywiński, Katarzyna Lewicka',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Zastosowanie cross-linking w stożku rogówki. Iwona Grabska-Liberek, Justyna Izdebska, Łukasz Kołodziejski, Marta Pietruszyńsk',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'ABC Presbiopii. Andrzej Grzybowski, Iwona Filipecka, Paweł Klonowski',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Szkła okularowe – rodzaje, właściwości fizyczne i zasady doboru u pacjentów. Marek Prost, Tomasz Tokarzewski',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'OCT i Angio-OCT w praktyce lekarskiej. Katarzyna Michalska- Małecka',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Oparzenia chemiczne i termiczne narządu wzroku. Bogumiła Wójcik- Niklewska',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Diagnostyka i leczenie orbitopatii tarczycowej. Mariusz Nowak, Dariusz Kajdaniuk, Dorota Pojda-Wilczek',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'RELEX SMILE – NAJNOWSZA METODA LASEROWEJ CHIRURGII REFRAKCYJNEJ. Danuta Horodyńska',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Wskazania do zabiegów witreoretinalnych, ze szczególnym uwzględnieniem schorzeń styku szklistkowo-siatkówkowego Jerzy Nawrocki, Rafał Leszczyński',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Zakazany Owoc -reklama lekarza. Paweł Jędrzejewski,Anna Maria Bilkiewicz-Pawelec Okulistyka Anna Maria Bilkiewicz-Pawelec',
                'price' => 50,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]
        ]);
    }
}
