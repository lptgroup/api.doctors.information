<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateGynecologyTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gynecology_topics', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->timestamps();
        });

        DB::table('gynecology_topics')->insert([
            [
                'name' => 'Położnictwo/perinatologia — nowe wyzwania',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'TTTS (ang. twin-to-twin transfusion syndrome czyli zespół przetoczenia krwi między płodami ) — wyniki leczenia w Polsce i na świecie',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Konflikty serologiczne — algorytm diagnostyczno-terapeutyczny',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Diagnostyka i leczenie ciąży w bliźnie po cięciu cesarskim',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Steroidoterapia w późnym wcześniactwie — korzyści i zagrożenia?',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Regulacja implantacji szansą na prawidłowy przebieg ciąży',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Nowe rekomendacje PTMRiE oraz PTGiP dotyczące diagnostyki i leczenia niepłodności',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Mała rezerwa jajnikowa, przedwczesna niewydolność jajników, słaba odpowiedź na stymulację- co łączy, co dzieli?',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Mrożenie tkanki jajnikowej w celu zabezpieczenia płodności — wskazania, metody pobierania, konserwowania, przeszczepiania. Doświadczenia światowe i polskie',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Plemniki do IVF — kiedy używać kriokonserwowanych, kiedy pochodzących z punkcji jądra?',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Postępy w ginekologii onkologicznej',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Co możemy zrobić, aby zapobiec zwiększeniu oporności na antybiotyki w leczeniu ZUM (zakażeń układu moczowego)?',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Rola endoskopii w diagnostyce i leczeniu niepłodności — aktualne zalecenia',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Możliwości i ograniczenia endoskopii w ginekologii onkologicznej',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Zaawansowane zabiegi endoskopowe w ginekologii onkologicznej',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Nadciśnienie tętnicze przewlekłe a ciąża — punkt widzenia hipertensjologa',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Nadciśnienie tętnicze przewlekłe a ciąża — punkt widzenia położnika',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Leczenie przeciwpadaczkowe a wady płodu',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Leczenie immunosupresyjne w ciąży',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => ' Poród u kobiety z wadą serca',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Nowe opcje terapeutyczne w starych problemach ginekologicznych',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Nowe techniki operacyjne — czy zawsze lepsze dla pacjentki?',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Miejsce ginekologii operacyjnej w diagnostyce i leczeniu zaburzeń endokrynologicznych',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Strategie postępowania w leczeniu mięśniaków macicy',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Uroginekologia — jak unikać odległych powikłań?',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Farmakoterapia w uroginekologii',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Co jest najważniejsze w leczeniu endometriozy?',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'PCOS (Zespół policystycznych jajników) u nastolatek — nowe kryteria diagnostyczne, kontrowersje w terapii',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Otyłość u ciężarnej — wyzwanie współczesnej opieki perinatalnej',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Czy jest szansa na dalszą redukcję wad rozwojowych u płodów matek z cukrzycą?',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Choroby tarczycy w ciąży — czy właściwie diagnozujemy?',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Choroba nowotworowa w ciąży — co możemy zrobić dla matki i dziecka?',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Profilaktyka i leczenie przeciwzakrzepowe w ciąży — u kogo i jak?',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => ' Antykoncepcja — czy nowe znaczy lepsze?',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Indukcja porodu — blaski i cienie',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Wskazania do indukcji porodu — kontrowersje',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gynecology_topics');
    }
}
