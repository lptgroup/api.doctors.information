<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GynecologySection extends Model
{
    protected $fillable = [
        'name'
      ];
}
