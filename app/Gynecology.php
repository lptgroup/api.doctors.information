<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gynecology extends Model
{
    protected $fillable = [
        'company', 'rx', 'otc', 'address', 'post_code', 'city',
        'phone1', 'phone2', 'website', 'email', 'description'
      ];
}
