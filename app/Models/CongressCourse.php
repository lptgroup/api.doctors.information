<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CongressCourse extends Model
{
    protected $fillable = [
        'congress_id', 'course_id'
      ];
}