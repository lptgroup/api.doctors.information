<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AcademicTitle extends Model
{
    protected $fillable = [
        'academic_title'
    ];

    public function stakeholders()
    {
        return $this->belongsTo(Stakeholder::class);
    }
}
