<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoursesHistory extends Model
{
    protected $fillable = [
        'congress_id', 
        'stakeholder_id', 
        'course_id'
    ];
}
