<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StakeholderCourse extends Model
{
    protected $fillable = [
        'stakeholder_id', 'course_id'
      ];
}
