<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stakeholder extends Model
{
    protected $fillable = [
        'first_name', 'last_name', 'email', 'pesel', 'pwz', 'phone',
        'sex', 'city', 'post_code', 'address', 'academic_title_id'
      ];

      protected $appends = ['isMember', 'isAffiliation'];

      public function courses()
        {
            return $this->belongsToMany(Course::class, 'stakeholder_courses');
        }

      public function congresses()
      {
          return $this->hasMany(Congress::class);
      }

      public function sections()
      {
          return $this->belongsToMany(Section::class, 'stakeholder_sections');
      }

      public function specializations()
      {
          return $this->belongsToMany(Specialization::class, 'stakeholder_specializations');
      }

      public function confirmations()
      {
          return $this->hasMany(Confirmation::class);
      }

      public function memberships()
      {
          return $this->hasMany(Membership::class);
      }

      public function getIsMemberAttribute()
      {
          return $this->memberships()->orderBy('created_at', 'DESC')->first();
      }

      public function affiliations()
      {
          return $this->hasMany(Affiliation::class);
      }

      public function getIsAffiliationAttribute()
      {
          return $this->affiliations()->orderBy('created_at', 'DESC')->first();
      }

      public function academic_title()
      {
          return $this->belongsTo(AcademicTitle::class, 'academic_title_id');
      }

      public function departments()
      {
          return $this->belongsToMany(Department::class, 'stakeholder_departments');
      }
}
