<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = [
        'section'
      ];

    public function stakeholders()
    {
        return $this->belongsTo(Stakeholder::class);
    }
}
