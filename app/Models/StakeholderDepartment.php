<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StakeholderDepartment extends Model
{
    protected $fillable = [
        'stakeholder_id', 'department_id'
      ];
}
