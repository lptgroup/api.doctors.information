<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payer extends Model
{
    protected $fillable = [
        'congress_id', 'type', 'name'
      ];

    public function congresses()
    {
        return $this->hasMany(Congress::class);
    }
}
