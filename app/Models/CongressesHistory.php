<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CongressesHistory extends Model
{
    protected $fillable = [
        'congress_id', 
        'stakeholder_id', 
        'status', 
        'payment_history_id'
      ];
}
