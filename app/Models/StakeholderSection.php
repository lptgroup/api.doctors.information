<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StakeholderSection extends Model
{
    protected $fillable = [
        'stakeholder_id', 'section_id'
      ];
}
