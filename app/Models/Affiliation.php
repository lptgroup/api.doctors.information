<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Affiliation extends Model
{
    protected $fillable = [
        'stakeholder_id', 'affiliation_name', 
        'affiliation_city', 'affiliation_post_code', 'affiliation_address'
      ];

    public function stakeholders()
    {
        return $this->hasMany(Stakeholder::class);
    }
}
