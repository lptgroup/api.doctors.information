<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Congress extends Model
{
    protected $fillable = [
        'stakeholder_id', 'congress_name'
    ];

    public function scopeAsVip($query)
    {
        return $query->where('status', 'Vip');
    }
    
    public function stakeholders()
    {
        return $this->hasMany(Stakeholder::class);
    }
}
