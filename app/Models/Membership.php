<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    protected $fillable = [
        'stakeholder_id', 'is_member', 'date_from'
      ];

    public function stakeholders()
    {
        return $this->hasMany(Stakeholder::class);
    }
}
