<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StakeholderSpecialization extends Model
{
    protected $fillable = [
        'stakeholder_id', 'specialization_id'
      ];
}
