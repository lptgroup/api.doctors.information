<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Confirmation extends Model
{
    protected $fillable = [
        'stakeholder_id', 'description'
      ];

    public function stakeholders()
    {
        return $this->belongsTo(Stakeholder::class);
    }
}
