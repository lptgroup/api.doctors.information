<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'congress_id', 'name', 'price'
      ];

    public function congresses()
    {
        return $this->belongsTo(Congress::class);
    }

    public function stakeholders()
    {
        return $this->belongsTo(Stakeholder::class);
    }
}
