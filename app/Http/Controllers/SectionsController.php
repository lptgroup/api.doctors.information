<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Section;

class SectionsController extends Controller
{
    public function index()
    {
        $sections = Section::get();
        \Log::info($sections);
        return $sections;
    }
}
