<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GynecologySection;

class GynecologySectionController extends Controller
{
    public function index()
    {
        $sections = GynecologySection::all();

        return view('gynecologies.sections.index', compact('sections'));
    }

    public function create()
    {
        return view('gynecologies.sections.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $gynecology = GynecologySection::create($input);

        session()->flash('createGynecology', 'Dodano nową pozycję');

        return back();
    }

    public function edit($id)
    {
        $gynecology = GynecologySection::find($id);

        return view('gynecologies.sections.edit', compact('gynecology'));
    }

    public function update(Request $request, $id)
    {
        $gynecology = GynecologySection::find($id);

        $gynecology->name = $request->input('name');

        $gynecology->save();

        session()->flash('editGynecology', 'Edytowano dane sekcji');

        return back();
    }

    public function delete($id)
    {
        $section = GynecologySection::find($id);
        $section->delete();

        session()->flash('destroyGynecology', 'Sekcja usunięta');

        return back();
    }
}
