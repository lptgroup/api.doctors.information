<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AcademicTitle;

class AcademictitleController extends Controller
{
    public function index()
    {
        $academic_title = AcademicTitle::get();
        \Log::info($academic_title);
        return $academic_title;
    }
}
