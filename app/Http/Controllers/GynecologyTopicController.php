<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GynecologyTopic;

class GynecologyTopicController extends Controller
{
    public function index()
    {
        $topics = GynecologyTopic::all();

        return view('gynecologies.topics.index', compact('topics'));
    }

    public function create()
    {
        return view('gynecologies.topics.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $gynecology = GynecologyTopic::create($input);

        session()->flash('createGynecology', 'Dodano nową pozycję');

        return back();
    }

    public function edit($id)
    {
        $gynecology = GynecologyTopic::find($id);

        return view('gynecologies.topics.edit', compact('gynecology'));
    }

    public function update(Request $request, $id)
    {
        $gynecology = GynecologyTopic::find($id);

        $gynecology->name = $request->input('name');

        $gynecology->save();

        session()->flash('editGynecology', 'Edytowano dane tematu');

        return back();
    }

    public function delete($id)
    {
        $section = GynecologyTopic::find($id);
        $section->delete();

        session()->flash('destroyGynecology', 'Temat usunięty');

        return back();
    }
}
