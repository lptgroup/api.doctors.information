<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Stakeholder;
use App\Models\Course;
use App\Models\Congress;
use App\Models\Section;
use App\Models\Specialization;
use App\Models\Confirmation;
use App\Models\Membership;
use App\Models\Affiliation;
use App\Models\Payer;
use App\Models\AcademicTitle;
use App\Models\Department;
use App\Models\StakeholderSpecialization;
use App\Models\StakeholderSection;
use App\Models\StakeholderDepartment;

class StakeholdersController extends Controller
{
    public function cancel($id)
    {
        $stakeholder = Stakeholder::find($id);
        $stakeholder->is_cancel = 1;
        $stakeholder->save();

        $msg = 'Anulowano';

        return $msg;
    }

    public function vipslist()
    {
        $vipBaseInfo = Stakeholder::with([
            'academic_title', 
            'specializations',
            'sections',
            'departments',
            'affiliations',
            'courses'])
        ->join('congresses', function($join) {
            $join->on('stakeholders.id', '=', 'congresses.stakeholder_id');
        })
        ->join('memberships', function($join) {
            $join->on('stakeholders.id', '=', 'memberships.stakeholder_id');
        })
        ->leftJoin('payers', function($join) {
            $join->on('congresses.id', '=', 'payers.congress_id');
        })
        ->where('status', 'Vip')
        ->where('is_cancel', 0)
        ->get();

        return $vipBaseInfo;
    }

    public function stakeholderslist()
    {
        $stakeholderBaseInfo = Stakeholder::with([
            'academic_title', 
            'specializations',
            'sections',
            'departments',
            'affiliations',
            'courses'])
        ->join('congresses', function($join) {
            $join->on('stakeholders.id', '=', 'congresses.stakeholder_id');
        })
        ->join('memberships', function($join) {
            $join->on('stakeholders.id', '=', 'memberships.stakeholder_id');
        })
        ->leftJoin('payers', function($join) {
            $join->on('congresses.id', '=', 'payers.congress_id');
        })
        ->where('status', 'uczestnik')
        ->where('is_cancel', 0)
        ->get();

        \Log::info($stakeholderBaseInfo);

        return $stakeholderBaseInfo;
    }

    public function show(Stakeholder $stakeholder)
    {
        \Log::info($stakeholder);
        $stakeholder
        ->load('academic_title')
        ->load('specializations')
        ->load('sections')
        ->load('departments')
        ->load('affiliations')
        ->load(['congresses' => function($query){
            $query
            ->with('courses')
            ->with('payers');
        }]);
        
        return $stakeholder;
    }

    public function edit(Stakeholder $stakeholder)
    {
        $stakeholder
        ->load('academic_title')
        ->load('specializations')
        ->load('sections')
        ->load('departments')
        ->load('affiliations')
        ->load(['congresses' => function($query){
            $query
            ->with('courses')
            ->with('payers');
        }]);
        
        foreach($stakeholder->departments as $k => $department) {
            $stakeholder->departments[$k] = $department->id;
        }

        foreach($stakeholder->specializations as $k => $specialization) {
            $stakeholder->specializations[$k] = $specialization->id;
        }

        foreach($stakeholder->sections as $k => $section) {
            $stakeholder->sections[$k] = $section->id;
        }

        \Log::info($stakeholder);

        return \Response::json($stakeholder, 200);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        \Log::info($input);

        if(isset($input['first_name']))
        {
            Stakeholder::where('id', $id)
            ->update([
                'first_name' => $input['first_name']
            ]);
        }

        if(isset($input['last_name']))
        {
            Stakeholder::where('id', $id)
            ->update([
                'last_name' => $input['last_name']
            ]);
        }

        if(isset($input['email']))
        {
            Stakeholder::where('id', $id)
            ->update([
                'email' => $input['email']
            ]);
        }

        if(isset($input['pesel']))
        {
            Stakeholder::where('id', $id)
            ->update([
                'pesel' => $input['pesel']
            ]);
        }

        if(isset($input['pwz']))
        {
            Stakeholder::where('id', $id)
            ->update([
                'pwz' => $input['pwz']
            ]);
        }

        if(isset($input['phone']))
        {
            Stakeholder::where('id', $id)
            ->update([
                'phone' => $input['phone']
            ]);
        }

        if(isset($input['sex']))
        {
            Stakeholder::where('id', $id)
            ->update([
                'sex' => $input['sex']
            ]);
        }

        if(isset($input['city']))
        {
            Stakeholder::where('id', $id)
            ->update([
                'city' => $input['city']
            ]);
        }

        if(isset($input['post_code']))
        {
            Stakeholder::where('id', $id)
            ->update([
                'post_code' => $input['post_code']
            ]);
        }

        if(isset($input['address']))
        {
            Stakeholder::where('id', $id)
            ->update([
                'address' => $input['address']
            ]);
        }

        if(isset($input['academic_title_id']))
        {
            Stakeholder::where('id', $id)
            ->update([
                'academic_title_id' => $input['academic_title_id']
            ]);
        }

        Membership::where('stakeholder_id', $id)
        ->update([
            'is_member' => $input['isMember']['is_member']
        ]);

        if(isset($input['isAffiliation']['affiliation_name']))
        {
            Affiliation::where('stakeholder_id', $id)
            ->update([
                'affiliation_name' => $input['isAffiliation']['affiliation_name'],
            ]);
        }

        if(isset($input['isAffiliation']['affiliation_city']))
        {
            Affiliation::where('stakeholder_id', $id)
            ->update([
                'affiliation_city' => $input['isAffiliation']['affiliation_city'],
            ]);
        }

        if(isset($input['isAffiliation']['affiliation_post_code']))
        {
            Affiliation::where('stakeholder_id', $id)
            ->update([
                'affiliation_post_code' => $input['isAffiliation']['affiliation_post_code']
            ]);
        }

        if(isset($input['isAffiliation']['affiliation_address']))
        {
            Affiliation::where('stakeholder_id', $id)
            ->update([
                'affiliation_address' => $input['isAffiliation']['affiliation_address']
            ]);
        }
        
        $stakeholder = Stakeholder::findOrFail($id);

        $congress = Congress::where('stakeholder_id', $id)->find($id);

        if(isset($input['congresses'][0]['payment']))
        {
            $congress->payment =  $input['congresses'][0]['payment'];
            $congress->save();
        }
        
        if(isset($input['congresses'][0]['payment_date']))
        {
            $congress->payment_date =  $input['congresses'][0]['payment_date'];
            $congress->save();
        }

        $payer = Payer::find($id);

        if(isset($input['congresses'][0]['payers'][0]['type']))
        {
            $payer->type = $input['congresses'][0]['payers'][0]['type'];
            $payer->save();
        }
        
        if(isset($input['congresses'][0]['payers'][0]['name']))
        {
            $payer->name = $input['congresses'][0]['payers'][0]['name'];
            $payer->save();
        }


        if(isset($input['congresses'][0]['courses']))
        {
            $congress->courses()->sync($input['congresses'][0]['courses']);
        }
        else
        {
            $congress->courses()->detach();
        }

        if(isset($input['congresses'][0]['courses']))
        {
            $stakeholder->courses()->sync($input['congresses'][0]['courses']);
        }
        else
        {
            $stakeholder->courses()->detach();
        }

        if(isset($input['specializations']))
        {
            $stakeholder->specializations()->sync($input['specializations']);
        }
        else
        {
            $stakeholder->specializations()->detach();
        }

        if(isset($input['sections']))
        {
            $stakeholder->sections()->sync($input['sections']);
        }
        else
        {
            $stakeholder->sections()->detach();
        }

        if(isset($input['departments']))
        {
            $stakeholder->departments()->sync($input['departments']);
        }
        else
        {
            $stakeholder->departments()->detach();
        }
    }

    public function store(Request $request)
    {
        $input = $request->all();
        \Log::info($input);
        
        $stakeholder = Stakeholder::create($input['stakeholder']);

        $input['congress']['stakeholder_id'] = $stakeholder->id;

        $congress = Congress::create($input['congress']);

        if(isset($input['congress']['selected_courses']))
        {
            foreach($input['congress']['selected_courses'] as $course)
            {
                $congress->courses()->attach($course);
            }
        }

        if(isset($input['stakeholder']['selected_courses']))
        {
            foreach($input['stakeholder']['selected_courses'] as $course)
            {
                $stakeholder->courses()->attach($course);
            }
        }

        if(isset($input['stakeholder']['selected_sections']))
        {
            foreach($input['stakeholder']['selected_sections'] as $section)
            {
                $stakeholder->sections()->attach($section);
            }
        }

        if(isset($input['stakeholder']['selected_specializations']))
        {
            foreach($input['stakeholder']['selected_specializations'] as $specialization)
            {
                $stakeholder->specializations()->attach($specialization);
            }
        }

        if(isset($input['stakeholder']['selected_departments']))
        {
            foreach($input['stakeholder']['selected_departments'] as $department)
            {
                $stakeholder->departments()->attach($department);
            }
        }

        if(isset($input['confirmation']['stakeholder_id']))
        {
            $input['confirmation']['stakeholder_id'] = $stakeholder->id;

            $confirmation = Confirmation::create($input['confirmation']);
        }
        
        $input['membership']['stakeholder_id'] = $stakeholder->id;

        $membership = Membership::create($input['membership']);

        $input['affiliation']['stakeholder_id'] = $stakeholder->id;

        $affiliation = Affiliation::create($input['affiliation']);

        $input['payer']['congress_id'] = $congress->id;

        $payer = Payer::create($input['payer']);
    }
}
