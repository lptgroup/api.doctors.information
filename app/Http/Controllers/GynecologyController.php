<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gynecology;

class GynecologyController extends Controller
{
    public function index()
    {
        $gynecologies = Gynecology::all();

        return view('gynecologies.index', compact('gynecologies'));
    }

    public function create()
    {
        return view('gynecologies.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $gynecology = Gynecology::create($input);

        session()->flash('createGynecology', 'Dodano nową pozycję');

        return back();
    }

    public function edit($id)
    {
        $gynecology = Gynecology::find($id);

        return view('gynecologies.edit', compact('gynecology'));
    }

    public function update(Request $request, $id)
    {
        $gynecology = Gynecology::find($id);

        $gynecology->company = $request->input('company');
        $gynecology->rx = $request->input('rx');
        $gynecology->otc = $request->input('otc');
        $gynecology->address = $request->input('address');
        $gynecology->post_code = $request->input('post_code');
        $gynecology->city = $request->input('city');
        $gynecology->phone1 = $request->input('phone1');
        $gynecology->phone2 = $request->input('phone2');
        $gynecology->website = $request->input('website');
        $gynecology->email = $request->input('email');
        $gynecology->description = $request->input('description');

        $gynecology->save();

        session()->flash('editGynecology', 'Edytowano dane');

        return back();
    }

    public function delete($id)
    {
        $gynecology = Gynecology::find($id);
        $gynecology->delete();

        session()->flash('destroyGynecology', 'Firma usunięta');

        return back();
    }
}
