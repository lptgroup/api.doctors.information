<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GynecologyTopic extends Model
{
    protected $fillable = [
        'name'
      ];
}
